<?php   
    require_once("../../config/Connect.php");
    if(isset($_SESSION["user_id"])){
    ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Catatan Perjalanan</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->


<?php require_once("../LayoutPartial/link.php") ?>


  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

<?php require_once("../LayoutPartial/header.php") ?>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->

<?php require_once("../LayoutPartial/nav.php") ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Catatan Perjalanan
        <small>semua ada di sini</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="../Home/index.php"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Konsultasi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
              <table id="ticket-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No Urut</th>
                  <th>Keadaan Tubuh</th>
                  <th>Lokasi</th>
                  <th>Status</th>
                  <th>tanggal</th>
                  <th>Pilihan</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

 <?php require_once("../LayoutPartial/script.php") ?>
 <script src="tiketKonsultasi.js" type="text/javascript"></script>
</body>
</html>

<?php
    }else{
        header("Location: ".Connect::base_url()."index.php");
    }
    ?>