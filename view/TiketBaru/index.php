<?php
  require_once("../../config/Connect.php");
  if(isset($_SESSION["user_id"])&&( $_SESSION['role_id'] != '2')){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>Isi Data</title>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">

    <!-- Link -->
    <?php require_once("../LayoutPartial/link.php"); ?>
    <!-- Header -->
    <?php require_once("../LayoutPartial/header.php"); ?>
    <!-- nav -->
    <?php require_once("../LayoutPartial/nav.php"); ?>


    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Halaman Tambah Data
            
        </h1>
        <ol class="breadcrumb">
            <li><a href="../Home/index.php"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Isi Data</a></li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">

    <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Baru</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="POST" action="" id="ticket-form" class="form-horizontal" autocomplete="off">
              <div class="box-body">
                <!-- User ID -->
                <input type="hidden" name="user_id" id="user_id" value="<?php echo $_SESSION["user_id"]?>">
                <!-- Kategori Tiket -->
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Keadaan Tubuh</label>
                  <div class="col-sm-6">
                    <select id="cat_select" name="cat_id" class="form-control">
                    </select>
                  </div>
                </div>
                <!-- Nama Tiket -->
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Lokasi yang Dikunjungi</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="tick_name" placeholder="Nama Tiket" name="tick_name">
                  </div>
                </div>
                <!-- Deskripsi Tiket -->
                <div class="form-group">
                  <label for="Deskripsi" class="col-sm-2 control-label">Lokasi Yang Dikunjungi</label>
                    
                  <div class="col-sm-8">
                  <textarea class="textarea" placeholder="..." id="textarea" name="tick_descrip"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="reset" class="btn btn-default">Batal</button>
                <button type="submit" name="action-save" value="save" class="btn btn-info">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
    <!-- /.box -->
</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- script -->
<?php require_once("../LayoutPartial/script.php"); ?>
<script src="tiketbaru.js" type="text/javascript"></script>
</body>
</html>
<?php
  }else{
    header("Location: ".Connect::base_url()."index.php");
  }
?>