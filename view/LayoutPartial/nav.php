<?php
  if($_SESSION["role_id"]==1){
    ?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">

      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
       
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">

      <li>
          <a href="..\Home\">
          <i class="fa fa-th"></i> <span>Home</span>
            </span>
          </a>
        </li>
        <li>
          <a href="..\TiketBaru\">
            <i class="fa fa-files-o"></i>
            <span>Isi Data</span>
            </span>
          </a>
        </li>
        <li>
          <a href="..\TiketKonsultasi\">
            <i class="fa fa-bed"></i>
            <span>Catatan Perjalanan</span>
            </span>
          </a>
        </li>
    <!-- /.sidebar -->
</aside>
    <?php
      }elseif($_SESSION["role_id"]==2){
    ?>
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">

      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
       
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">

      <li>
          <a href="..\Home\">
          <i class="fa fa-th"></i> <span>Home</span>
            </span>
          </a>
        </li>
        <li>
          <a href="..\TiketKonsultasi\">
            <i class="fa fa-bed"></i>
            <span>Konsultasi</span>
            </span>
          </a>
        </li>
    <!-- /.sidebar -->
  </aside>
    <?php
      }else{
    ?>
     <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">

      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
       
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">

      <li>
          <a href="..\Home\">
          <i class="fa fa-th"></i> <span>Home</span>
            </span>
          </a>
        </li>
        <li>
          <a href="..\TiketKonsultasi\">
            <i class="fa fa-bed"></i>
            <span>Konsultasi</span>
            </span>
          </a>
        </li>
        <li>
          <a href="..\KelolaPengguna\">
            <i class="fa fa-book"></i>
            <span>Kelola Pengguna</span>
            </span>
          </a>
        </li>
    <!-- /.sidebar -->
  </aside>

  <?php
      }
  ?>