<header class="main-header">
    <!-- Logo -->
    <a href="../Home/index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>PE</b>DO</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Peduli</b>Diri</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        <input type="hidden" id="user_id" value="<?php echo $_SESSION["user_id"]?>">
        <input type="hidden" id="role_id" value="<?php echo $_SESSION["role_id"]?>">
        
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              
            <span>Welcome</span>
            <span>
                <?php echo $_SESSION["user_nickname"]; ?>
            </span>

            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header col-sm-11 my-0">
                <p>Pengaturan Pengguna
                  <small><?php echo $_SESSION["user_email"]; ?></small>

                  <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="..\Logout\logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
                <div class="text-center">
                  <a href="#" class="btn btn-default btn-flat">Setting</a>
                </div>
                </p>
              </li>
              <!-- Menu Body -->

              <!-- Menu Footer-->

              <li class="user-footer">
                
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>